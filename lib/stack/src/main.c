#include <stdio.h>
#include <string.h>

#include "dq_memory.h"
#include "dq_stack.h"

struct DQStack {
    int n, i;
    int *a; /* array */
};

int dq_stack_ini(/**/ int n, DQStack **pq) {
    DQStack *q;
    EMALLOC(1, &q);
    EMALLOC(n, &q->a);
    q->n = n; q->i = 0;
    *pq = q;
    return STACK_OK;
}

int dq_stack_fin(DQStack *q) {
    EFREE(q->a);
    EFREE(q);
    return STACK_OK;
}

int dq_stack_reset(DQStack *q) {
    q->i = 0;
    return STACK_OK;
}

int dq_stack_empty(DQStack *q) { return q->i == 0; }
int dq_stack_pop(DQStack *q, int *po) {
    int i, *a;
    i = q->i; a = q->a;
    if (i == 0) return STACK_EMPTY;
    *po = a[--i];
    q->i = i;
    return STACK_OK;
}

int dq_stack_push(DQStack *q, int b) {
    int n, i, *a;
    n = q->n; i = q->i; a = q->a;
    if (i == n) return STACK_SIZE;
    a[i++] = b;
    q->i = i;
    return STACK_OK;
}
