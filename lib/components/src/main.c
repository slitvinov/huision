#include <stdio.h>
#include <stdlib.h>

#include "dq_memory.h"
#include "dq_stack.h"
#include "dq_components.h"

#define T DQComponents
#define I DQComponentsInput

typedef struct Stack {
    DQStack *w, *h;
} Stack;
static void stack_ini(int maxn, Stack *stack) {
    dq_stack_ini(maxn, &stack->w);
    dq_stack_ini(maxn, &stack->h);
}
static void stack_fin(Stack *stack) {
    dq_stack_fin(stack->w);
    dq_stack_fin(stack->h);
}
static void stack_reset(Stack *stack) {
    dq_stack_reset(stack->w);
    dq_stack_reset(stack->h);
}
static int stack_push(Stack *stack, int w, int h) {
    int r;
    r = dq_stack_push(stack->w, w);
    if (r != STACK_OK) return r;
    r = dq_stack_push(stack->h, h);
    return r;
}
static int stack_empty(Stack *stack) {
    return dq_stack_empty(stack->w);
}
static int stack_pop(Stack *stack, int *pw, int *ph) {
    int r;
    dq_stack_pop(stack->w, pw);
    r = dq_stack_pop(stack->h, ph);
    return r;
}

struct T {
    int maxn, maxc; /* max number of pixels and components */
    unsigned char *foreground;
    int *labels;
    long *wwl, *hhl;

    int *ww, *hh; /* center of mass of the compoennts */
    int *nn;      /* number of pixels in the components */
    Stack stack;
};

typedef struct Field {
    int width, height;
    unsigned char *foreground;
    int *labels;
} Field;
static int idx(int w, int h, int width) { return h*width + w; }
static void field_ini(T *q, const I *input, /**/ Field *f) {
    f->width  = dq_components_input_width(input);
    f->height = dq_components_input_height(input);
    f->foreground = q->foreground;
    f->labels = q->labels;
}
static int field_n(Field *f) { return f->width * f->height; }
static int field_width(Field *f) { return f->width; }
static int field_height(Field *f) { return f->height; }
static void field_zero(Field *f, const I *input) {
    int i, n, width, height, w, h, val, *labels;
    unsigned char *foreground;
    width = f->width; height = f->height; labels = f->labels; foreground = f->foreground;
    n = width * height;
    for (i = 0; i < n; i++)
        labels[i] = 0;

    for (w = 0; w < width; w++)
        for (h = 0; h < height; h++) {
            i = idx(w, h, width);
            dq_components_input_foreground(input, w, h, &val);
            foreground[i] = val;
        }
}
static int field_emptyp(Field *f, int w, int h) {
    enum {FULL = 0, EMPTY = 1};
    int width, height, i;
    unsigned char *foreground;
    int *labels;
    width = f->width; height = f->height;
    foreground = f->foreground;
    labels = f->labels;
    if (w <  0 || w >= width)  return FULL;
    if (h <  0 || h >= height) return FULL;
    i = idx(w, h, width);
    if (labels[i] != 0) return FULL;
    if (foreground[i]) return EMPTY;
    else               return FULL;
}
void field_set(Field *f, int w, int h, int component) {
    int width, i;
    int *labels;
    width = f->width;
    labels = f->labels;
    i = idx(w, h, width);
    labels[i] = component;
}

int dq_components_ini(int maxn, int maxc, /**/ T **pq) {
    T *q;
    EMALLOC(1, &q);
    EMALLOC(maxn, &q->labels);
    EMALLOC(maxn, &q->foreground);

    EMALLOC(maxc, &q->ww); EMALLOC(maxc, &q->wwl);
    EMALLOC(maxc, &q->hh); EMALLOC(maxc, &q->hhl);

    EMALLOC(maxc, &q->nn);

    stack_ini(maxn, &q->stack);
    q->maxc = maxc;
    q->maxn = maxn;
    *pq = q;
    return COMPONENTS_OK;
}

int dq_components_fin(T *q) {
    EFREE(q->labels);
    EFREE(q->foreground);
    EFREE(q->ww); EFREE(q->hh);
    EFREE(q->wwl); EFREE(q->hhl);
    EFREE(q->nn);
    stack_fin(&q->stack);
    EFREE(q);
    return COMPONENTS_OK;
}

static void dfs(Field *field, int w, int h, Stack *stack, int component) {
    int r;
    stack_reset(stack);
    r = stack_push(stack, w, h);
    if (r != STACK_OK) {
        fprintf(stderr, "stack is full\n");
        return;
    }
    while (!stack_empty(stack)) {
        stack_pop(stack, &w, &h);
        if (field_emptyp(field, w, h)) {
            field_set(field, w, h, component);
            stack_push(stack, w - 1, h - 1);
            stack_push(stack, w - 1, h + 0);
            stack_push(stack, w - 1, h + 1);

            stack_push(stack, w + 0, h - 1);
            stack_push(stack, w + 0, h + 1);

            stack_push(stack, w + 1, h - 1);
            stack_push(stack, w + 1, h + 0);
            stack_push(stack, w + 1, h + 1);
        }
    }
}
static void components(Stack *stack, /**/ int *pncomp, Field *field) {
    int width, height, w, h, component;
    width = field_width(field);
    height = field_height(field);
    component = 1;
    for (w = 0; w < width; w++)
        for (h = 0; h < height; h++) {
            if (!field_emptyp(field, w, h)) continue;
            dfs(field, w, h, stack, component++);
        }
    *pncomp = component;
}

int dq_components_label(T *q, const I *input, /**/ int *pncomp, const int **plabels) {
    int maxn, n, ncomp;
    Field f;
    maxn = q->maxn;
    field_ini(q, input, &f);
    n = field_n(&f);
    if (n > maxn) {
        fprintf(stderr, "n=%d > maxn=%d\n", n, maxn);
        return COMPONENTS_SIZE;
    }
    field_zero(&f, input);
    components(&q->stack, /**/ &ncomp, &f);

    *pncomp  = ncomp;
    *plabels = q->labels;
    return COMPONENTS_OK;
}


int dq_components_com(T *q, const I *input, /**/ int *pncomp, const int **pnn, const int **pww, const int **phh) {
    int i, ncomp, n, maxc, w, h;
    int width, height, label;
    const int *labels;
    long *ww, *hh;
    int *nn;
    ww = q->wwl; hh = q->hhl; nn = q->nn; maxc = q->maxc;
    width = dq_components_input_width(input);
    height = dq_components_input_height(input);

    dq_components_label(q, input, &ncomp, &labels);
    if (ncomp > maxc) {
        fprintf(stderr, "%s:%d: ncomp=%d > maxc=%d\n", __FILE__, __LINE__, ncomp, maxc);
        return COMPONENTS_SIZE;
    }

    for (i = 0; i < ncomp; i++)
        ww[i] = hh[i] = nn[i] = 0;

    for (w = 0; w < width; w++)
        for (h = 0; h < height; h++) {
            i = idx(w, h, width);
            label = labels[i];
            ww[label] += w;
            hh[label] += h;
            nn[label] ++;
        }

    for (i = 0; i < ncomp; i++) {
        n = nn[i];
        if (n == 0) {
            fprintf(stderr, "n == 0 for component %d\n", i);
            return COMPONENTS_INTERNAL;
        }
        ww[i] /= n; hh[i] /= n;
    }

    for (i = 0; i < ncomp; i++) {
        q->ww[i] = ww[i]; q->hh[i] = hh[i];
    }

    *pncomp = ncomp;
    *pnn = q->nn; *pww = q->ww; *phh = q->hh;

    return COMPONENTS_OK;
}
