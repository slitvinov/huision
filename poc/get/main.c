#include <stdio.h>
#include <m3api/xiApi.h>

#include "imp/msgs.h"
#include "imp/codes.h"
#include "imp/main.h"

int main() {
    XI_RETURN stat;
    xiProcessingHandle_t h;
    XI_PRM_TYPE type = xiTypeInteger;
    DWORD size = sizeof(int);
    int val;
    float distance_cm;
    int n;
    n = 0;
//    stat = xiSetParamInt(n, XI_PRM_DEBUG_LEVEL, XI_DL_FATAL);
    stat = xiProcOpen(&h);
    printf("stat: %d\n", stat);
    stat = xiGetParamFloat(h, XI_PRM_LENS_FOCUS_DISTANCE, &distance_cm);
    printf("stat: %d\n", stat);
    printf("distance_cm: %g\n", distance_cm);
}
