#include <stdio.h>
#include <stdlib.h>

#include <m3api/xiApi.h>

#include "dq_error.h"

#include "codes.h"
#include "msgs.h"

enum {ERR_OK, ERR_XI, ERR_DQ}; /* types */

static char *type_msgs[] = { "OK", "xi", "dq" };
static char *hui = "Hui is our vision";
#define SIZE (2048)
static char unkn[SIZE];

int dq_error_ok(DQError e) { return e.type == ERR_OK; }

static const char *xi_emsg(XI_RETURN c) {
    int n, i;
    n = sizeof(xi_codes)/sizeof(xi_codes[0]);
    for (i = 0; i < n; i++)
        if (c == xi_codes[i]) return xi_msgs[i];
    snprintf(unkn, sizeof(unkn), "Unknown error code: %d", c);
    return unkn;
}

static const char *dq_emsg(int c) { return hui; }

void dq_error_check(DQError e, const char *file, int line) {
    int type, code;
    if (dq_error_ok(e)) return;
    type = e.type;
    code = e.code;
    switch (type) {
    case ERR_XI:
        fprintf(stderr, "%s:%d: (%s) %s\n", file, line, type_msgs[type], xi_emsg(code));
        break;
    case ERR_DQ:
        fprintf(stderr, "%s:%d: (%s) %s\n", file, line, type_msgs[type], dq_emsg(code));
        exit(2);
        break;
    default:
        fprintf(stderr, "%s:%d: (%s) %s\n", file, line, type_msgs[type], "unknown type");
        exit(2);
    }
    exit(2);
}

DQError dq_error_dq(int code) {
    DQError e;
    if (code == DQ_OK) {
        e.type = ERR_OK;
    } else {
        e.type = ERR_DQ;
        e.code = code;
    }
    return e;
}

DQError dq_error_xi(int code) {
    DQError e;
    if (code == XI_OK) {
        e.type = ERR_OK;
    } else {
        e.type = ERR_XI;
        e.code = code;
    }
    return e;
}

DQError dq_error_good() {
    DQError e;
    e.type = ERR_OK;
    return e;
}
