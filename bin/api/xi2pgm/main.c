#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dq_error.h"
#include "dq_api.h"

const char *prog = "dq.xi2pgm";

int eq(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}

void usg0() {
    fprintf(stderr, "usage: %s IN.xi OUT.pgm\n", prog);
}
void usg(int c, const char *v[]) {
    if (c < 2) return;
    if (eq(v[1], "-h")) {
        usg0();
        exit(0);
    }
}

void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

int main(int argc, const char *argv[]) {
    int i, id;
    DQImage  *image;
    char in[FILENAME_MAX], out[FILENAME_MAX];

    usg(argc, argv);
    atos(--argc, *++argv, in);
    atos(--argc, *++argv, out);    

    DQ_CHECK(dq_image_read(in, &image));
    fprintf(stderr, "%s: %s\n", prog, out);
    DQ_CHECK(dq_image_pgm(image, out));
    DQ_CHECK(dq_image_fin(image));
}
