#include <stdio.h>
#include <string.h>
#include <m3api/xiApi.h>
#include <m3api/xiapi_dng_store.h>

#include "imp.h"

int main() {
    XI_RETURN stat;
    HANDLE xiH;
    DWORD NumberDevices;
    DWORD TimeOut = 5000;
    XI_IMG img;
    XI_DNG_METADATA metadata;
    float distance_cm;

    stat = xiGetNumberDevices(&NumberDevices);
    if (!good(stat)) {
        fprintf(stderr, ": %s\n", emsg(stat));
        exit(2);
    }
    fprintf(stderr, "number of devices: %d\n", NumberDevices);

    stat = xiOpenDevice(0, &xiH);
    if (!good(stat)) {
        fprintf(stderr, ": %s\n", emsg(stat));
        exit(2);
    }

    stat = xiSetParamInt(xiH, XI_PRM_DEBUG_LEVEL, XI_DL_FATAL);
    if (!good(stat)) {
        fprintf(stderr, ": %s\n", emsg(stat));
        exit(2);
    }
    
    stat = xiSetParamInt(xiH, XI_PRM_EXPOSURE, 200 * 1000);
    if (!good(stat)) {
        fprintf(stderr, ": %s\n", emsg(stat));
        exit(2);
    }

    memset(&img,0,sizeof(img));
    img.size = sizeof(XI_IMG);
    stat = xiStartAcquisition(xiH);
    if (!good(stat)) {
        fprintf(stderr, ": %s\n", emsg(stat));
        exit(2);
    }

    stat = xiGetImage(xiH, TimeOut, &img);
    if (!good(stat)) {
        fprintf(stderr, ": %s\n", emsg(stat));
        exit(2);
    }

    xidngFillMetadataFromCameraParams(xiH, &metadata);
    stat = xidngStore("test.dng", &img, &metadata);
        if (!good(stat)) {
        fprintf(stderr, ": %s\n", emsg(stat));
        exit(2);
    }

    stat = xiStopAcquisition(xiH);
    if (!good(stat)) {
        fprintf(stderr, ": %s\n", emsg(stat));
        exit(2);
    }

    stat = xiCloseDevice(xiH);
    if (!good(stat)) {
        fprintf(stderr, ": %s\n", emsg(stat));
        exit(2);
    }
    puts(": end");
}
