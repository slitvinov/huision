#include <stdlib.h>
#include "dq_memory.h"

void emalloc(size_t size, /**/ void **data) {
    *data = malloc(size);
}

void efree(void *ptr) {
    free(ptr);
    ptr = NULL;
}
