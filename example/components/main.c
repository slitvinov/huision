#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dq_memory.h"
#include "dq_pgm.h"
#include "dq_img_pgm.h"
#include "dq_components.h"

static const char *prog = "dq.pgm";

static const int COLORS[] = {0, 50, 100, 150, 200, 250};
static const int maxc = sizeof(COLORS)/sizeof(COLORS[0]);

static void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

static int whitep(DQComponentsInput *input, int w, int h) {
    int o, s;
    s = dq_components_input_foreground(input, w, h, /**/ &o);
    if (s != COMPONENTS_INPUT_OK) {
        fprintf(stderr, "dq_components_input_foreground failed\n");
        exit(2);
    }
    return o;
}
static void main0(DQComponentsInput *input, /**/ unsigned char *out) {
    DQComponents *components;
    int s, w, h, i, cnt, n, ncomp, width, height, white, black, label;
    const int *labels, *nn, *ww, *hh;

    width  = dq_components_input_width(input);
    height = dq_components_input_height(input);
    fprintf(stderr, "width=%d height=%d\n", width, height);
    white = black = 0;
    for (w = 0; w < width; w++)
        for (h = 0; h < height; h++)
            if (whitep(input, w, h)) white++; else black++;
    fprintf(stderr, "w: %d%%\n", 100*white/(white + black));

    n = width * height;
    dq_components_ini(n, maxc, &components);
    s = dq_components_label(components, input, &ncomp, &labels);
    s = dq_components_com(components, input, /**/ &ncomp, &nn, &ww, &hh);
    for (i = 0; i < ncomp; i++)
        printf("%d %d %d\n", nn[i], ww[i], hh[i]);


    for (cnt = i = 0; i < n; i++) {
        label = labels[i];
        if (labels != 0) cnt++;
        out[i] = COLORS[label];
    }
    fprintf(stderr, "ncomp: %d\n", ncomp);
    fprintf(stderr, "com: %d%%\n", 100*cnt / n);
    dq_components_fin(components);
}

int main(int argc, const char *argv[]) {
    int threshold;
    char i[FILENAME_MAX], o[FILENAME_MAX];
    DQpgm *pgm;
    DQComponentsInput *components_input;

    int width, height, n;
    const void *data;
    unsigned char *labels;

    atos(--argc, *++argv, i);
    atos(--argc, *++argv, o);

    dq_pgm_ini(&pgm);
    if (dq_pgm_read(pgm, i) != PGM_OK) {
        fprintf(stderr, "%s: fail to read '%s'\n", prog, i);
        exit(2);
    }
    width = dq_pgm_width(pgm);
    height = dq_pgm_height(pgm);
    data   = dq_pgm_data(pgm);
    n = width * height;

    threshold = 100;
    dq_components_input_threshold(width, height, data, threshold,
                                  /**/ &components_input);
    EMALLOC(n, &labels);
    main0(components_input, /**/ labels);
    dq_components_input_fin(components_input);

    if (dq_img_pgm(width, height, labels, o) != IMG_PGM_OK) {
        fprintf(stderr, "%s: failt to write '%s'\n", prog, o);
        exit(2);
    };
    dq_pgm_fin(pgm);
}
