struct DQDevice {
    HANDLE H;
    char path[FILENAME_MAX];
};

enum {NONE, CAMERA, DISK}; /* where image is comming from ? */
struct DQImage {
    XI_IMG I;
    int source;
};
