typedef struct DQpgm DQpgm;

enum {PGM_OK, PGM_IO, PGM_SIZE};
int dq_pgm_ini(/**/ DQpgm**);
int dq_pgm_fin(DQpgm*);

int dq_pgm_read(DQpgm*, const char *path);
int dq_pgm_width(DQpgm*);
int dq_pgm_height(DQpgm*);
const void *dq_pgm_data(DQpgm*);

int dq_pgm_maxval(DQpgm*);
