from skimage import measure
from scipy import misc
import pylab as plt
import numpy as np
import glob
import os


segdirname = 'segments/'
os.system('mkdir '+segdirname)

def crop_image(fname, threshold=100, frame_d=65):
    a = misc.imread(fname)
    # mask elements by threshold
    mask = np.where(a>threshold, 1, 0)
    ll_labels, num = measure.label(mask, return_num=True)
    #iterate over all connected components
    for i in range(num):
        ar = np.where(ll_labels==i+1)
        #find center of the component
        centrx = int((max(ar[0])+min(ar[0]))/2.)
        centry = int((max(ar[1])+min(ar[1]))/2.)
        # check if the center in the focus. focus is a round
        if (centry-968)**2+(centrx-608)**2 <= 520**2:
            seg = a[centrx-frame_d:centrx+frame_d,centry-frame_d:centry+frame_d]
            imnum = int(fname.split('/')[-2]+fname.split('/')[-1].split('.png')[0])
            misc.imsave(segdirname+'segment_{}_{}.png'.format(imnum,i),seg)

li1 = glob.glob('../05_04_18_dataset/healthy/1/*png')
li2 = glob.glob('../05_04_18_dataset/healthy/2/*png')
li = li1+li2

for ifname in li:
    crop_image(ifname)
