typedef struct DQDevice DQDevice;
typedef struct DQImage  DQImage;

typedef struct DQConf {
    int exposure;
    int gain;
    char path[FILENAME_MAX];
} DQConf ;

DQError dq_get_number_device(int*);
DQError dq_open_device(int, DQConf, DQDevice**);
DQError dq_close_device(DQDevice*);

DQError dq_start_acquisition(DQDevice*);
DQError dq_stop_acquisition(DQDevice*);

DQError dq_image_ini(DQImage**);
DQError dq_image_read(const char *path, DQImage **pq);
DQError dq_image_fin(DQImage*);

DQError dq_image_log(DQImage*);
DQError dq_get_image(DQDevice*, int timeout, DQImage*);

DQError dq_image_store(DQImage*, DQDevice*, int id);
DQError dq_image_xi(DQImage*, DQDevice*, int id);
DQError dq_image_pgm(DQImage*, const char *path);

int dq_image_width(DQImage*);
int dq_image_height(DQImage*);
const void *dq_image_data(DQImage*);
