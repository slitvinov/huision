#include <stdio.h>
#include "dq_error.h"

DQError fun() { return dq_error_dq(42); }
int main() {
    DQError e;
    e = fun();
    DQ_CHECK(e);
}
