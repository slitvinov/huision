static char unkn[2048];
const char *emsg(XI_RETURN c) {
    int n, i;
    n = sizeof(codes)/sizeof(codes[0]);
    for (i = 0; i < n; i++)
        if (c == codes[i]) return msgs[i];
    snprintf(unkn, sizeof(unkn), "Unknown error code: %d", c);
    return unkn;
}

int good(XI_RETURN c) { return c == XI_OK; }
