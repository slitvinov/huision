# input:
# TRG, HFILES, OFILES
# CFLAGS, DQ_CFLAGS, CC

LIB = lib$(TRG).a
PC  = $(TRG).pc
SED = sed

$(LIB): $(OFILES); ar rv $@ $(OFILES) && ranlib $@
clean:; rm -f $(OFILES) $(LIB) $(PC)
%.o: src/%.c; $(CC) -Iinc -Isrc $(CFLAGS) $(DQ_CFLAGS) -c -o $@ $<

install: $(LIB) $(HFILES) $(PC)
	mkdir -p $(PREFIX)/lib $(PREFIX)/include $(PREFIX)/pkgconfig
	cp $(LIB)    $(PREFIX)/lib
	cp $(PC)     $(PREFIX)/pkgconfig
	cp $(HFILES) $(PREFIX)/include

$(PC): main.pc
	$(SED) -e "s,@PREFIX@,$(PREFIX),g" \
               -e "s,@LIB@,$(LIB),g" \
               -e "s,@TRG@,$(TRG),g" \
        $< > $@

.PHONY: clean install
