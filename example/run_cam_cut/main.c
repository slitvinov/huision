#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dq_memory.h>
#include <dq_pgm.h>
#include <dq_error.h>
#include <dq_api.h>
#include <dq_img_pgm.h>
#include <dq_components.h>
#include <dq_cut.h>

static const char *prog = "dq.cut";

static int eq(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}

static int mkdir(const char *d) {
    char cmd[FILENAME_MAX];
    if (snprintf(cmd, FILENAME_MAX, "mkdir -p '%s'", d) < 0) {
        fprintf(stderr, "snprintf faild\n");
        exit(2);
    }

    if (system(cmd) != 0) {
        fprintf(stderr, "fail to run: %s\n", cmd);
        exit(2);
    }
}

static void usg0() {
    fprintf(stderr, "%s IN.pgm OUT.DIR\n", prog);
    fprintf(stderr, "dumps OUT.DIR/00001.pgm OUT.DIR/00002.pgm ...\n");
    fprintf(stderr, "create OUT.DIR/log\n");
    exit(0);
}

FILE *fopen_log(const char *path, const char *mode) {
    FILE *f;
    fprintf(stderr, "open: '%s'\n", path);
    if ( (f = fopen(path, mode)) == NULL) {
        fprintf(stderr, "%s: fail to open file '%s'\n", prog, path);
        exit(2);
    }
    return f;
}

static void usg(int c, const char *v[]) {
    if (c >= 2 && eq(v[1], "-h")) usg0();
}

int atoi0(int c, const char *s) {
    int ans;
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    if (sscanf(s, "%d", &ans) != 1) {
        fprintf(stderr, "not an integer '%s'\n", s);
        exit(2);
    };
    return ans;
}

static void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fprintf(stderr, "%s: not enough arguments\n", prog);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

int cut_one_image(char o[FILENAME_MAX],
		  const void *input,
		  int threshold,
		  int id_img,
		  int width,
		  int height,
		  int width0,
		  int height0,
		  DQComponentsInput *components_input,
		  DQComponents *components,
		  DQCut *cut) {
    int s, j;
    unsigned char **output;
    const int *cut_status;
    char name[FILENAME_MAX];
    int n, ncomp;
    const int *nn, *ww, *hh;
    const char *msg;

    s = dq_components_com(components, components_input, /**/
                          &ncomp, &nn, &ww, &hh);
    if (s != COMPONENTS_OK) {
        fprintf(stderr, "dq_components_com failed\n");
        exit(2);
    }
    dq_cut_apply(cut, width, height, input, ncomp, ww, hh, /**/
                 &output, &cut_status);
    for (j = 1; j < ncomp; j++) {
        snprintf(name, FILENAME_MAX, "%s/%05d_%03d.pgm", o, id_img, j);
        // msg = dq_cut_status2msg(cut_status[j]);
        // fprintf(stderr, "%s [%d x %d] %d\n",  msg, ww[j], hh[j], nn[j]);
        if (cut_status[j] != CUT_STATUS_IN) continue;
        fprintf(stderr, "dump: '%s'\n", name);
        if (dq_img_pgm(width0, height0, output[j], name) != IMG_PGM_OK) {
            fprintf(stderr, "%s: fail to write '%s'\n", prog, name);
            exit(2);
        }
    }

}

int main(int argc, const char *argv[]) {
  // example of usage: ./main out 200000 50 0 100  out
  const void *input;
  DQpgm *pgm;
  int width, height, N, timeout_ms, num_dev, n, npix, threshold, id, max_component;
  int width0, height0;
  char o[FILENAME_MAX];
  DQImage  *image;
  DQDevice *device;
  DQConf conf;

  DQComponentsInput *components_input;
  DQComponents *components;
  DQCut *cut;
  if (argc < 2) {
    fputs("mssing directory name\n", stderr);
    exit(2);
  }

  // get args
  usg(argc, argv);
  strncpy(conf.path, *++argv, FILENAME_MAX);
  conf.exposure = atoi0(--argc, *++argv);
  threshold = atoi0(--argc, *++argv);
  conf.gain     = atoi0(--argc, *++argv);
  N        = atoi0(--argc, *++argv);
  fprintf(stdout, "start with params N:%d gain:%d exposure:%d \n",
	  N, conf.gain, conf.exposure);
  atos(--argc, *++argv, o);
  
  // create out directory
  mkdir(o);
  
  // get image from device
  DQ_CHECK(dq_get_number_device(&n));
  if (n < 1) {
    fputs(": no device found\n", stderr);
    exit(-2);
  }
  DQ_CHECK(dq_open_device(num_dev, conf, &device));
  DQ_CHECK(dq_image_ini(&image));
  DQ_CHECK(dq_start_acquisition(device));
  timeout_ms = 500;

  // get image width and height from the first image
  DQ_CHECK(dq_get_image(device, timeout_ms, image));
  width = dq_image_width(image);
  height = dq_image_height(image);
  input = dq_image_data(image);
  npix = width * height;
  
  //init components and cut
  max_component = 100;
  width0 = 150;
  height0 =  150;
  dq_components_input_threshold(width, height, input, threshold,
				/**/ &components_input);
  dq_components_ini(npix, max_component, &components);
  dq_cut_ini(max_component, width0, height0, /**/ &cut);


  for (id = 0; id < N; id++) {
    DQ_CHECK(dq_get_image(device, timeout_ms, image));  
    input = dq_image_data(image);
    // cut image
    cut_one_image(o, input, threshold, id, width, height, width0, height0,
		  components_input, components, cut);
  }

  //finalize components and cut
  dq_cut_fin(cut);
  dq_components_fin(components);
  dq_components_input_fin(components_input);

  //finalize image
  DQ_CHECK(dq_image_fin(image));
}
