global v
v = argv();

function shift()
  global v
  v = v(2:end);
endfunction

function img = imread0(path)
  img = imread(path);
  img = int64(img);
endfunction

function imwrite0(img, filename)
  img = uint8(img);
  imwrite(img, filename);
endfunction

function b = zeros0(a)
  sz = size(a); cls = class(a);
  b = zeros(sz);
  b = cast(b, cls);
endfunction

function img = ramp(img)
  lo = min(img(:)); hi = max(img(:));
  lo0 = 0; hi0 = 255;
  img = (img - lo) / hi;
  img = lo0 + img * (hi0 - lo0);
endfunction

n = 0;
f = v{1}; shift();
img = imread0(f);
sum = img; sum2 = img.^2; n++;
while numel(v) ~= 0
  f = v{1}; shift();
  img = imread0(f);
  sum += img; sum2 += img.^2; n++;
endwhile
mean  = sum/n;
var   = sum2/n - sum/n;

imwrite0(mean,       "mean.pgm");
imwrite0(ramp(var),  "var.pgm");
