enum {IMG_IO_OK, IMG_IO_FAIL};
int dq_img_io_write(const XI_IMG*, const char*);
int dq_img_io_read(const char*, XI_IMG*);
