#include <string.h>
#include <stdio.h>

#include <m3api/xiApi.h>
#include <m3api/xiapi_dng_store.h>

#include "dq_error.h"
#include "dq_memory.h"
#include "dq_img_io.h"
#include "dq_img_desc.h"
#include "dq_img_format.h"
#include "dq_img_log.h"
#include "dq_img_pgm.h"

#include "dq_api.h"

#include "type.h"

#define SIZE 2048
#define DEBUG_LEVEL XI_DL_DISABLED

DQError dq_get_number_device(int *pn) {
    DQError s;
    int n;
    s = dq_error_xi(xiGetNumberDevices(&n));
    *pn = n;
    return s;
}

static DQError set(HANDLE H, const char *param, int val) {
    DQError s;
    int val0;
    s = dq_error_xi(xiSetParamInt(H, param,  val));
    if (!dq_error_ok(s)) return s;

    s = dq_error_xi(xiGetParamInt(H, param,  &val0));
    if (!dq_error_ok(s)) return s;
    fprintf(stderr, "%s: %d : %d\n", param, val, val0);
    return s;
}

DQError dq_open_device(int n, DQConf conf, DQDevice **pq) {
    DQDevice *q;
    DQError s;
    HANDLE H;
    int exposure, gain;

    EMALLOC(1, &q);
    exposure = conf.exposure; gain = conf.gain;
    strncpy(q->path, conf.path, FILENAME_MAX);

    s = dq_error_xi(xiOpenDevice(n, &H));
    if (!dq_error_ok(s)) return s;

    s = set(H, XI_PRM_DEBUG_LEVEL, DEBUG_LEVEL);
    if (!dq_error_ok(s)) return s;

    s = set(H, XI_PRM_EXPOSURE,  exposure);
    if (!dq_error_ok(s)) return s;

    s = set(H, XI_PRM_GAIN,  gain);
    if (!dq_error_ok(s)) return s;

    q->H = H;
    *pq = q;
    return s;
}

DQError dq_close_device(DQDevice *q) {
    DQError s;
    s = dq_error_xi(xiCloseDevice(q->H));
    EFREE(q);
    return s;
}

DQError dq_start_acquisition(DQDevice *q) {
    HANDLE H;
    H = q->H;
    return dq_error_xi(xiStartAcquisition(H));
}

DQError dq_stop_acquisition(DQDevice *q) {
    HANDLE H;
    H = q->H;
    return dq_error_xi(xiStopAcquisition(H));
}

DQError dq_image_ini(DQImage **pq) {
    DQImage *q;
    EMALLOC(1, &q);
    memset(&q->I, 0, sizeof(q->I));
    q->I.size = sizeof(XI_IMG);
    q->source = NONE;
    *pq = q;
    return dq_error_good();
}

DQError dq_image_read(const char *path, DQImage **pq) {
    DQImage *q;
    EMALLOC(1, &q);
    if (dq_img_io_read(path, &q->I) != IMG_IO_OK)
        return dq_error_dq(DQ_IO);
    q->source = DISK;
    *pq = q;
    return dq_error_good();
}

DQError dq_image_fin(DQImage *q) {
    if (q->source == DISK)
        EFREE(q->I.bp);
    EFREE(q);
    return dq_error_good();
}

DQError dq_get_image(DQDevice *q, int timeout, DQImage *i) {
    HANDLE H;
    H = q->H;
    return dq_error_xi(xiGetImage(H, timeout, &i->I));
}

DQError dq_image_log(DQImage *q) {
    XI_IMG_DESC desc;
    XI_IMG_FORMAT format;
    const char *name = "img_format";
    const char *val;
    desc = q->I.img_desc;
    format = q->I.frm;
    dq_img_log(&q->I);
    dq_img_desc2log(desc);

    val = dq_img_format2name(format);
    fprintf(stderr, "%-25s %s\n", name, val);
    return dq_error_good();
}

DQError dq_image_store(DQImage *q, DQDevice *d, int id) {
    HANDLE H;
    XI_DNG_METADATA metadata;
    char path[SIZE];
    DQError s;
    H = d->H;
    snprintf(path, SIZE, "%s/%05d.dng", d->path, id);
    fprintf(stderr, "%s\n", path);
    xidngFillMetadataFromCameraParams(H, &metadata);
    s = dq_error_xi(xidngStore(path, &q->I, &metadata));
    return s;
}


DQError dq_image_xi(DQImage *q, DQDevice *d, int id) {
    XI_DNG_METADATA metadata;
    char path[SIZE];
    snprintf(path, SIZE, "%s/%05d.xi", d->path, id);
    fprintf(stderr, "%s\n", path);
    if (dq_img_io_write(&q->I, path) != IMG_IO_OK)
        return dq_error_dq(DQ_IO);
    else return dq_error_good();
}

DQError dq_image_pgm(DQImage *q, const char *path) {
    int width, height, n, bp_size, frm;
    const void *bp;

    width = q->I.width;
    height = q->I.height;
    n = width * height;
    bp = q->I.bp;
    bp_size = q->I.bp_size;
    frm = q->I.frm;
    
    if (bp_size != n) {
        fprintf(stderr, "bp_size=%d != n=%d\n", bp_size, n);
        return dq_error_dq(DQ_IMG);
    }
    if (frm != XI_MONO8) {
        fprintf(stderr, "frm=%s != XI_MONO8\n", dq_img_format2name(frm));
        return dq_error_dq(DQ_IMG);        
    }

    if (dq_img_pgm(width, height, bp, path) != IMG_PGM_OK)
        return dq_error_dq(DQ_IO);
    else return dq_error_good();
}

int dq_image_width(DQImage *q) { return q->I.width;}
int dq_image_height(DQImage *q) { return q->I.height;}
const void *dq_image_data(DQImage *q) { return q->I.bp; }
/* int dq_pgm_height(DQpgm *q) { return q->height; } */
/* const void *dq_pgm_data(DQpgm *q) { return q->data; } */

  
