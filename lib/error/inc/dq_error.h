enum {DQ_OK, DQ_IO, DQ_IMG};

typedef struct DQError {
    int type;
    int code;
} DQError;

#define DQ_CHECK(e) \
    dq_error_check(e, __FILE__, __LINE__)

int dq_error_ok(DQError);
void dq_error_check(DQError, const char *file, int line);

DQError dq_error_dq(int code);
DQError dq_error_xi(int code);
DQError dq_error_good();
