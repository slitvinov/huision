#include <stdio.h>

#include <m3api/xiApi.h>
#include "dq_img_io.h"
#include "dq_img_desc.h"

static const char *path = "o.xi";

void write() {
    int status;
    const XI_IMG img;
    dq_img_desc2log(img.img_desc);
    status = dq_img_io_write(&img, path);
    if (status != IMG_IO_OK) {
        fprintf(stderr, "failed to write '%s'\n", path);
        exit(-2);
    }
}

void read() {
    int status;
    XI_IMG img;
    dq_img_desc2log(img.img_desc);
    status = dq_img_io_read(path, &img);
    if (status != IMG_IO_OK) {
        fprintf(stderr, "failed to read '%s'\n", path);
        exit(-2);
    }
}

int main() {
    write();
    read();
}
