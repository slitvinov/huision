import numpy as np
import scipy.ndimage as sp
import matplotlib.pyplot as mp
import pylab as plt
import glob

#sp.filters.laplace()

li = glob.glob("focus_measured/*png")
a = mp.imread(li[1])

fil = sp.filters.laplace(a, cval=0.0)
# plt.imshow(fil,'gray')
# plt.show()


data = [(29, 25.5, 16),
        (36, 32 , 20),
        (38.8, 33.5, 21),
        (40.2,35,22),
        (48, 42, 26),
        (52.4, 45.5,28.5),
        (54.3, 47, 29.5),
        (57, 50, 31.5)]
coef = 1936/1216.
diameter_mentos = 1.4
fig, ax = plt.subplots()
dist = [i[0] for i in data]
length = [i[1]/2. for i in data]
width = [i[2]/2. for i in data]
res = [diameter_mentos/i*1216 for i in width]
ax.scatter(dist, width, label = 'length field of view')

for i, resi in enumerate(res):
    txt = '{}x{}'.format(round(resi),round(resi))
    ax.annotate(txt, (dist[i],width[i]))
plt.legend()
plt.xlabel("distance to object in cm")
plt.ylabel("width field of view distance in cm")
plt.show()
