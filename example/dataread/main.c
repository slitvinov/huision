#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dq_memory.h>
#include <dq_pgm.h>
#include <dq_error.h>
#include <dq_api.h>
#include <dq_img_pgm.h>
#include <dq_components.h>

const char *prog = "dq.read";

static const int COLORS[] = {0, 50, 100, 150, 200, 250};
static const int maxc = sizeof(COLORS)/sizeof(COLORS[0]);

static int eq(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}

static void usg0() {
    fprintf(stderr, "usage: %s FILE.xi\n", prog);
}
static void usg(int c, const char *v[]) {
    if (c < 2) return;
    if (eq(v[1], "-h")) {
        usg0();
        exit(0);
    }
}

int atoi0(int c, const char *s) {
    int ans;
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    if (sscanf(s, "%d", &ans) != 1) {
        fprintf(stderr, "not an integer '%s'\n", s);
        exit(2);
    };
    return ans;
}

static void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

static int whitep(DQComponentsInput *input, int w, int h) {
    int o, s;
    s = dq_components_input_foreground(input, w, h, /**/ &o);
    if (s != COMPONENTS_INPUT_OK) {
        fprintf(stderr, "dq_components_input_foreground failed\n");
        exit(2);
    }
    return o;
}

static void main0(DQComponentsInput *input, /**/ unsigned char *out) {
    DQComponents *components;
    int s, w, h, i, cnt, n, ncomp, width, height, white, black, label;
    const int *labels, *nn, *ww, *hh;

    width  = dq_components_input_width(input);
    height = dq_components_input_height(input);
    fprintf(stderr, "width=%d height=%d\n", width, height);
    white = 0; black = 0;
    for (w = 0; w < width; w++)
      for (h = 0; h < height; h++)
	if (whitep(input, w, h)) white++; else black++;
    
    fprintf(stderr, "w: %d%%\n", 100*white/(white + black + 1));
    
    n = width * height;
    dq_components_ini(n, maxc, &components);
    s = dq_components_label(components, input, &ncomp, &labels);
    s = dq_components_com(components, input, /**/ &ncomp, &nn, &ww, &hh);
    for (i = 0; i < ncomp; i++)
        printf("%d %d %d\n", nn[i], ww[i], hh[i]);

    for (cnt = i = 0; i < n; i++) {
        label = labels[i];
	//printf("preved\n");
        if (labels != 0) cnt++;
	//printf("w: %d %d %d %d\n", i, n, out[i], label);
        out[i] = COLORS[label];
	//printf("preved2\n");
    }
    
    fprintf(stderr, "ncomp: %d\n", ncomp);
    fprintf(stderr, "com: %d%%\n", 100*cnt / n);
    dq_components_fin(components);
}

int main(int argc, const char *argv[]) {
    int i, id, w, h, n, threshold, N, timeout_ms;
    const void *data;
    unsigned char *labels;
    DQImage  *image;
    DQComponentsInput *components_input;
    DQConf conf;
    DQDevice *device;
    char o[FILENAME_MAX];
    //char path[FILENAME_MAX];
    //usg(argc, argv);
    //atos(--argc, *++argv, path);

    // read image
    //DQ_CHECK(dq_image_read(path, &image));
    if (argc < 2) {
        fputs("mssing directory name\n", stderr);
        exit(2);
    }
    
    strncpy(conf.path, *++argv, FILENAME_MAX); --argc;
    conf.exposure = atoi0(--argc, *++argv);
    conf.gain     = atoi0(--argc, *++argv);
    N        = atoi0(--argc, *++argv);
    atos(--argc, *++argv, o);
    
    DQ_CHECK(dq_get_number_device(&n));
    if (n < 1) {
        fputs(": no device found\n", stderr);
        exit(-2);
    }

    i = 0;
    DQ_CHECK(dq_open_device(i, conf, &device));
    DQ_CHECK(dq_image_ini(&image));
    DQ_CHECK(dq_start_acquisition(device));

    // initialize something
    threshold = 80;

    // main loop
    timeout_ms = 5000;
    for (id = 0; id < N; id++) {
        DQ_CHECK(dq_get_image(device, timeout_ms, image));
	// image stats
	w = dq_image_width(image);
	h = dq_image_height(image);	
	data = dq_image_data(image);
	dq_components_input_threshold(w, h, data, threshold,
				      &components_input);

	n = w * h;
	EMALLOC(n, &labels);

	// return components
	main0(components_input, /**/ labels);

	// save image and destroy pointers
	dq_components_input_fin(components_input);
	if (dq_img_pgm(w, h, labels, o) != IMG_PGM_OK) {
	  fprintf(stderr, "%s: failt to write '%s'\n", prog, o);
	  exit(2);
	};

    }

    DQ_CHECK(dq_image_fin(image));
}
