#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dq_pgm.h"
#include "dq_img_pgm.h"

const char *prog = "dq.pgm";

static void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

int main(int argc, const char *argv[]) {
    char i[FILENAME_MAX], o[FILENAME_MAX];
    DQpgm *pgm;
    int width, height;
    const void *data;
    atos(--argc, *++argv, i);
    atos(--argc, *++argv, o);
    
    dq_pgm_ini(&pgm);
    if (dq_pgm_read(pgm, i) != PGM_OK) {
        fprintf(stderr, "%s: fail to read '%s'\n", prog, i);
        exit(2);
    }
    
    width = dq_pgm_width(pgm);
    height = dq_pgm_height(pgm);
    data   = dq_pgm_data(pgm);

    if (dq_img_pgm(width, height, data, o) != IMG_PGM_OK) {
        fprintf(stderr, "%s: failt to write '%s'\n", prog, o);
        exit(2);
    };
    
    dq_pgm_fin(pgm);
}
