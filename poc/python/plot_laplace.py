import numpy as np
import scipy.ndimage as sp
import matplotlib.pyplot as mp
import pylab as plt
import glob
import os

li = glob.glob("focus_measured/*png")
outdir = 'laplace'
os.system('mkdir '+outdir)
for n,i in enumerate(li):
    fig, ax = plt.subplots(1, 2, figsize=(20, 10))
    a = mp.imread(i)
    fil = sp.filters.laplace(a, cval=0.0)
    fname = i.split('/')[-1]
    ax[0].imshow(a,'gray')
    ax[1].imshow(fil,'gray')
    plt.title('distance to camera: '+fname.split('.png')[0].replace('_','.'))
    plt.savefig(outdir+'/laplace_'+fname, dpi=200)
