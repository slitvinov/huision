typedef struct DQStack DQStack;

enum {STACK_OK, STACK_SIZE, STACK_EMPTY};
int dq_stack_ini(/**/ int n, DQStack**);
int dq_stack_fin(DQStack*);

int dq_stack_reset(DQStack*);
int dq_stack_empty(DQStack*);
int dq_stack_pop(DQStack*, /**/ int*);
int dq_stack_push(DQStack*, int);
