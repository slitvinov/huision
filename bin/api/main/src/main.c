#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dq_error.h"
#include "dq_api.h"

int atoi0(int c, const char *s) {
    int ans;
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    if (sscanf(s, "%d", &ans) != 1) {
        fprintf(stderr, "not an integer '%s'\n", s);
        exit(2);
    };
    return ans;
}

int main(int argc, const char *argv[]) {
    int N;
    int n, i, id, timeout_ms;
    DQDevice *device;
    DQImage  *image;
    DQConf conf;

    if (argc < 2) {
        fputs("mssing directory name\n", stderr);
        exit(2);
    }
    strncpy(conf.path, *++argv, FILENAME_MAX); --argc;
    conf.exposure = atoi0(--argc, *++argv);
    conf.gain     = atoi0(--argc, *++argv);
    N        = atoi0(--argc, *++argv);
    
    DQ_CHECK(dq_get_number_device(&n));
    if (n < 1) {
        fputs(": no device found\n", stderr);
        exit(-2);
    }

    i = 0;
    DQ_CHECK(dq_open_device(i, conf, &device));
    DQ_CHECK(dq_image_ini(&image));
    DQ_CHECK(dq_start_acquisition(device));

    timeout_ms = 5000;
    for (id = 0; id < N; id++) {
        DQ_CHECK(dq_get_image(device, timeout_ms, image));
        DQ_CHECK(dq_image_store(image, device, id));
    }

    DQ_CHECK(dq_stop_acquisition(device));
    DQ_CHECK(dq_image_fin(image));
    DQ_CHECK(dq_close_device(device));
}
