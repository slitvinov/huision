#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dq_error.h"
#include "dq_api.h"

const char *prog = "dq.read";

static int eq(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}

static void usg0() {
    fprintf(stderr, "usage: %s FILE.xi\n", prog);
}
static void usg(int c, const char *v[]) {
    if (c < 2) return;
    if (eq(v[1], "-h")) {
        usg0();
        exit(0);
    }
}

static void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

int main(int argc, const char *argv[]) {
    int i, id;
    DQImage  *image;
    char path[FILENAME_MAX];

    usg(argc, argv);
    atos(--argc, *++argv, path);

    DQ_CHECK(dq_image_read(path, &image));
    DQ_CHECK(dq_image_log(image));
    DQ_CHECK(dq_image_fin(image));
}
