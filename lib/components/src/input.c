#include <stdio.h>
#include <stdlib.h>

#include "dq_memory.h"
#include "dq_components.h"

#define T DQComponentsInput

#define MAX_VAL (256)

struct T {
    int width, height, threshold;
    const unsigned char *data;
};

int dq_components_input_threshold(int width, int height, const unsigned char *data, int threshold,
                                  /**/ T **pq) {
    T *q;
    EMALLOC(1, &q);
    q->width = width; q->height = height;
    q->data = data; q->threshold = threshold;
    *pq = q;
    return COMPONENTS_INPUT_OK;
}
int dq_components_input_fin(T *q) { EFREE(q); return COMPONENTS_INPUT_OK; }
int dq_components_input_foreground(const T *q, int w, int h, /**/ int *po) {
    enum {BACKGROUND, FOREGROUND};
    int width, height, threshold, val;
    const unsigned char *data;

    width = q->width; height = q->height; threshold = q->threshold; data = q->data;
    if (w < 0 || w >= width) {
        fprintf(stderr, "wrong w = %d, width = %d\n", w, width);
        return COMPONENTS_INPUT_OVER;
    }

    if (h < 0 || h >= width) {
        fprintf(stderr, "wrong h = %d, height = %d\n", h, height);
        return COMPONENTS_INPUT_OVER;
    }
    val = data[h*width + w];
    if (val < 0 || val >= MAX_VAL) {
        fprintf(stderr, "wrong val = %d max_val = %d\n", val, MAX_VAL);
        return COMPONENTS_INPUT_OVER;
    }
    *po = (val > threshold) ? FOREGROUND : BACKGROUND;
    return COMPONENTS_INPUT_OK;
}

int dq_components_input_width(const T *q) { return q->width; }
int dq_components_input_height(const T *q) { return q->height; }

