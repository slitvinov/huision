#include <stdio.h>

#include <m3api/xiApi.h>

#include "dq_img_io.h"

int dq_img_io_write(const XI_IMG *img, const char *path) {
    size_t nmemb;
    int bp_size;
    const void *bp;
    FILE *f;
    f = fopen(path, "w");
    if (f == NULL) return IMG_IO_FAIL;

    bp_size = img->bp_size; bp = img->bp;
    if (bp == NULL) return IMG_IO_FAIL;
    fprintf(stderr, "bp_size : %d\n", bp_size);
    nmemb = 1;
    if (fwrite(img, sizeof(img[0]), nmemb, f) != nmemb)
        return IMG_IO_FAIL;
    if (fwrite(bp, bp_size, nmemb, f) != nmemb)
        return IMG_IO_FAIL;
    fclose(f);
    return IMG_IO_OK;
}

int dq_img_io_read(const char *path, XI_IMG *img) {
    size_t nmemb;
    int bp_size;
    FILE *f;
    f = fopen(path, "r");
    if (f == NULL) return IMG_IO_FAIL;

    nmemb = 1;
    if (fread(img, sizeof(img[0]), nmemb, f) != nmemb)
        return IMG_IO_FAIL;

    bp_size = img->bp_size;
    img->bp = malloc(bp_size);
    if (img->bp == NULL) return IMG_IO_FAIL;

    if (fread(img->bp, bp_size, nmemb, f) != nmemb)
        return IMG_IO_FAIL;
    
    fclose(f);
    return IMG_IO_OK;
}
