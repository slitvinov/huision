#include <stdio.h>
#include "dq_img_pgm.h"

int dq_img_pgm(int width, int height, const void *buf, const char *path) {
    FILE *f;
    const char *magic = "P5";
    int maxval = 255;
    int n, i, nmemb;
    f = fopen(path, "w");
    if (f == NULL) {
        fprintf(stderr, "cannot write to file '%s'\n", path);
        return IMG_PGM_FAIL;
    }

    fprintf(f, "%s\n", magic);
    fprintf(f, "%d %d\n", width, height);
    fprintf(f, "%d\n", maxval);
    n = width * height;
    nmemb = 1;
    fwrite(buf, n, nmemb, /**/ f);
    fclose(f);
    return IMG_PGM_OK;
}
