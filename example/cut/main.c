#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dq_memory.h"
#include "dq_pgm.h"
#include "dq_img_pgm.h"
#include "dq_components.h"
#include "dq_cut.h"

static const char *prog = "dq.cut";

static int eq(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}

static int mkdir(const char *d) {
    char cmd[FILENAME_MAX];
    if (snprintf(cmd, FILENAME_MAX, "mkdir -p '%s'", d) < 0) {
        fprintf(stderr, "snprintf faild\n");
        exit(2);
    }

    if (system(cmd) != 0) {
        fprintf(stderr, "fail to run: %s\n", cmd);
        exit(2);
    }
}

static void usg0() {
    fprintf(stderr, "%s IN.pgm OUT.DIR\n", prog);
    fprintf(stderr, "dumps OUT.DIR/00001.pgm OUT.DIR/00002.pgm ...\n");
    fprintf(stderr, "create OUT.DIR/log\n");
    exit(0);
}

FILE *fopen_log(const char *path, const char *mode) {
    FILE *f;
    fprintf(stderr, "open: '%s'\n", path);
    if ( (f = fopen(path, mode)) == NULL) {
        fprintf(stderr, "%s: fail to open file '%s'\n", prog, path);
        exit(2);
    }
    return f;
}

static void usg(int c, const char *v[]) {
    if (c >= 2 && eq(v[1], "-h")) usg0();
}

static void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fprintf(stderr, "%s: not enough arguments\n", prog);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

int main(int argc, const char *argv[]) {
    int threshold, s, j;
    unsigned char **output;
    const int *cut_status;
    char i[FILENAME_MAX], o[FILENAME_MAX], name[FILENAME_MAX], log_name[FILENAME_MAX];
    DQpgm *pgm;
    DQCut *cut;
    DQComponentsInput *components_input;
    DQComponents *components;

    int width, height, width0, height0, max_component, n, ncomp;
    const void *input;
    const int *nn, *ww, *hh;
    const char *msg;
    FILE *log;

    threshold = 50; /* <threshold is background */
    max_component = 1000;
    width0 = height0 = 150; /* size of the compunent */

    usg(argc, argv);
    atos(--argc, *++argv, i);
    atos(--argc, *++argv, o);

    dq_pgm_ini(&pgm);
    if (dq_pgm_read(pgm, i) != PGM_OK) {
        fprintf(stderr, "%s: fail to read '%s'\n", prog, i);
        exit(2);
    }
    width = dq_pgm_width(pgm);
    height = dq_pgm_height(pgm);
    input   = dq_pgm_data(pgm);
    n = width * height;
    dq_components_input_threshold(width, height, input, threshold,
                                  /**/ &components_input);
    dq_components_ini(n, max_component, &components);
    s = dq_components_com(components, components_input, /**/
                          &ncomp, &nn, &ww, &hh);
    if (s != COMPONENTS_OK) {
        fprintf(stderr, "dq_components_com failed\n");
        exit(2);
    }
    dq_cut_ini(max_component, width0, height0, /**/ &cut);
    dq_cut_apply(cut, width, height, input, ncomp, ww, hh, /**/
                 &output, &cut_status);

    if (ncomp > 0) {
        mkdir(o);
        snprintf(log_name, FILENAME_MAX, "%s/log", o);
        log = fopen_log(log_name, "w");
    }
    for (j = 1; j < ncomp; j++) {
        snprintf(name, FILENAME_MAX, "%s/%05d.pgm", o, j);
        msg = dq_cut_status2msg(cut_status[j]);
        fprintf(stderr, "%s [%d x %d] %d\n",  msg, ww[j], hh[j], nn[j]);
        if (cut_status[j] != CUT_STATUS_IN) continue;
        fprintf(stderr, "dump: '%s'\n", name);
        if (dq_img_pgm(width0, height0, output[j], name) != IMG_PGM_OK) {
            fprintf(stderr, "%s: fail to write '%s'\n", prog, name);
            exit(2);
        }
        fprintf(log, "%s %d %d %d\n", name, ww[j], hh[j], nn[j]);
    }
    fclose(log);

    dq_cut_fin(cut);
    dq_components_fin(components);
    dq_components_input_fin(components_input);
    dq_pgm_fin(pgm);
}
