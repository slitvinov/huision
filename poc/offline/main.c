#include <stdio.h>
#include <m3api/xiApi.h>

int main() {
    XI_RETURN stat;
    xiProcessingHandle_t h;
    XI_PRM_TYPE type;
    DWORD size;
    void *val;
    const char* prm = "preved";
    
    stat = xiProcOpen(&h);
    printf("stat: %d\n", stat);
    
    stat = xiProcSetParam(&h, prm, val, size, type);
    printf("stat: %d\n", stat);    
}
