#include <stdio.h>

#include <m3api/xiApi.h>
#include "dq_img_format.h"

int main() {
    const char *msg, *name;
    XI_IMG_FORMAT f;
    f = XI_RAW16X4;
    msg = dq_img_format2msg(f);
    name = dq_img_format2name(f);
    fprintf(stderr, "msg: %s\n", msg);
    fprintf(stderr, "name: %s\n", name);
}
