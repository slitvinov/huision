#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dq_error.h"
#include "dq_api.h"

const char *prog = "dq.write";

int eq(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}

void usg0() {
    fprintf(stderr, "usage: %s directory exposure gain\n", prog);
}
void usg(int c, const char *v[]) {
    if (c < 2) return;
    if (eq(v[1], "-h")) {
        usg0();
        exit(0);
    }
}

int atoi0(int c, const char *s) {
    int ans;
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    if (sscanf(s, "%d", &ans) != 1) {
        fprintf(stderr, "not an integer '%s'\n", s);
        exit(2);
    };
    return ans;
}

void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

int main(int argc, const char *argv[]) {
    int n, i, id, timeout_ms;
    DQDevice *device;
    DQImage  *image;
    DQConf conf;

    usg(argc, argv);

    atos(--argc, *++argv, conf.path);
    conf.exposure = atoi0(--argc, *++argv);
    conf.gain     = atoi0(--argc, *++argv);

    DQ_CHECK(dq_get_number_device(&n));
    if (n < 1) {
        fputs(": no device found\n", stderr);
        exit(-2);
    }

    i = 0;
    DQ_CHECK(dq_open_device(i, conf, &device));
    DQ_CHECK(dq_image_ini(&image));
    DQ_CHECK(dq_start_acquisition(device));

    timeout_ms = 5000; id = 0;
    DQ_CHECK(dq_get_image(device, timeout_ms, image));
    DQ_CHECK(dq_image_log(image));
    DQ_CHECK(dq_image_xi(image, device, id));

    DQ_CHECK(dq_stop_acquisition(device));
    DQ_CHECK(dq_image_fin(image));
    DQ_CHECK(dq_close_device(device));
}
