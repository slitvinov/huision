#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dq_memory.h>
#include <dq_pgm.h>
#include <dq_error.h>
#include <dq_api.h>
#include <dq_img_pgm.h>
#include <dq_components.h>

const char *prog = "dq.read";

static const int COLORS[] = {0, 50, 100, 150, 200, 250};
static const int maxc = sizeof(COLORS)/sizeof(COLORS[0]);

static int eq(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}

static void usg0() {
    fprintf(stderr, "usage: %s FILE.xi\n", prog);
}
static void usg(int c, const char *v[]) {
    if (c < 2) return;
    if (eq(v[1], "-h")) {
        usg0();
        exit(0);
    }
}

int atoi0(int c, const char *s) {
    int ans;
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    if (sscanf(s, "%d", &ans) != 1) {
        fprintf(stderr, "not an integer '%s'\n", s);
        exit(2);
    };
    return ans;
}

static void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

int main(int argc, const char *argv[]) {
    int i, id, w, h, n, threshold, N, timeout_ms;
    const void *data;
    unsigned char *labels;
    DQImage  *image;
    DQComponentsInput *components_input;
    DQConf conf;
    DQDevice *device;
    char o[FILENAME_MAX];
    
    if (argc < 2) {
        fputs("mssing directory name\n", stderr);
        exit(2);
    }
    
    strncpy(conf.path, *++argv, FILENAME_MAX); --argc;
    conf.exposure = atoi0(--argc, *++argv);
    conf.gain     = atoi0(--argc, *++argv);
    N        = atoi0(--argc, *++argv);
    atos(--argc, *++argv, o);
    
    DQ_CHECK(dq_get_number_device(&n));
    if (n < 1) {
        fputs(": no device found\n", stderr);
        exit(-2);
    }

    i = 0;
    DQ_CHECK(dq_open_device(i, conf, &device));
    DQ_CHECK(dq_image_ini(&image));
    DQ_CHECK(dq_start_acquisition(device));
    timeout_ms = 5000;
    DQ_CHECK(dq_get_image(device, timeout_ms, image));
    w = dq_image_width(image);
    h = dq_image_height(image);
    data = dq_image_data(image);

    if (dq_img_pgm(w, h, data, o) != IMG_PGM_OK) {
        fprintf(stderr, "%s: failt to write '%s'\n", prog, o);
        exit(2);
    };
    
    DQ_CHECK(dq_image_fin(image));
}
