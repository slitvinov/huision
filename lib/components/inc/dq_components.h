#define T DQComponents
#define I DQComponentsInput

typedef struct T T;
typedef struct I I;

enum {COMPONENTS_OK, COMPONENTS_SIZE, COMPONENTS_INTERNAL};
int dq_components_ini(int maxn, int maxc, /**/ T**);
int dq_components_fin(T*);
int dq_components_label(T*, const I*, /**/ int *ncomp, const int **labels);

/* number of pixels and centers of mass of the components */
int dq_components_com(T*, const I*, /**/ int *ncomp, const int **nn, const int **ww, const int **hh);

enum {COMPONENTS_INPUT_OK, COMPONENTS_INPUT_THRESHOLD, COMPONENTS_INPUT_OVER};
int dq_components_input_threshold(int width, int height, const unsigned char *data, int threshold, /**/ I**);
int dq_components_input_fin(I*);
int dq_components_input_foreground(const I*, int w, int h, /**/ int *o);
int dq_components_input_width(const I*);
int dq_components_input_height(const I*);

#undef T
#undef I
