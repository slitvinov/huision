typedef struct DQCut DQCut;

enum {CUT_OK, CUT_SIZE, CUT_EMPTY, CUT_UNINIT};
int dq_cut_ini(int maxc, int width, int height, /**/ DQCut**);
int dq_cut_fin(DQCut*);

enum {CUT_STATUS_IN, CUT_STATUS_OUT};
int dq_cut_apply(DQCut*,
                 int width, int height, const unsigned char *input,
                 int ncom, const int *ww, const int *hh,
                 /**/ unsigned char **output[], const int **status);
const char* dq_cut_status2msg(int);
