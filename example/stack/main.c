#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dq_stack.h"

static void atos(int c, const char *s, char *p) {
    if (c == 0) {
        fputs("not enough arguments\n", stderr);
        exit(2);
    }
    strncpy(p, s, FILENAME_MAX);
}

int main(int argc, const char *argv[]) {
    char i[FILENAME_MAX];
    int n, out;
    DQStack *stack;
    atos(--argc, *++argv, i);
    n = 100;
    dq_stack_ini(n, /**/ &stack);

    dq_stack_push(stack, 30);
    dq_stack_push(stack, 10);

    dq_stack_pop(stack, &out);
    fprintf(stderr, "%d\n", out);

    dq_stack_pop(stack, &out);
    fprintf(stderr, "%d\n", out);

    dq_stack_push(stack, 10);    
    fprintf(stderr, "empty = %d\n", dq_stack_empty(stack));

    dq_stack_pop(stack, &out);
    fprintf(stderr, "%d\n", out);

    dq_stack_fin(stack);
}
