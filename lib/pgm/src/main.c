#include <stdio.h>
#include <string.h>

#include "dq_memory.h"
#include "dq_pgm.h"

#define MAX_SIZE (4000*4000)
#define MAX_VAL  (255)

struct DQpgm {
    void *data;
    int width, height, maxval;
};

int dq_pgm_ini(DQpgm **pq) {
    DQpgm *q;
    EMALLOC(1, &q);
    EMALLOC(MAX_SIZE, &q->data);
    *pq = q;
    return PGM_OK;
}

int dq_pgm_fin(DQpgm *q) {
    EFREE(q->data);
    EFREE(q);
    return PGM_OK;
}

static int eq(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}
int dq_pgm_read(DQpgm *q, const char *path) {
    FILE *f;
    char s[4048];
    int width, height, maxval, n, nmemb;
    const char *magic = "P5";
    f = fopen(path, "r");
    if (f == NULL) return PGM_IO;

    if (fscanf(f, "%s", s) != 1) return PGM_IO;
    if (!eq(s, magic)) {
        fprintf(stderr, "%s != %s\n", s, magic);
        return PGM_IO;
    }
    if (fscanf(f, "%d %d", &width, &height) != 2)
        return PGM_IO;

    n = width * height;
    if (n > MAX_SIZE) {
        fprintf(stderr, "n=%d > MAX_SIZE=%d\n", n, MAX_SIZE);
        return PGM_SIZE;
    }
    if (fscanf(f, "%d", &maxval) != 1)
        return PGM_IO;

    if (maxval != MAX_VAL) {
        fprintf(stderr, "maxval=%d > MAX_VAL=%d\n", n, MAX_VAL);
        return PGM_IO;
    }

    nmemb = 1;
    if (fread(q->data, n, nmemb, f) != nmemb) {
        fprintf(stderr, "fail to read data");
        return PGM_IO;
    }
    q->width = width; q->height = height; q->maxval = maxval;
    fclose(f);
    return PGM_OK;
}

int dq_pgm_width(DQpgm *q) { return q->width; }
int dq_pgm_height(DQpgm *q) { return q->height; }
const void *dq_pgm_data(DQpgm *q) { return q->data; }
