#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dq_memory.h"
#include "dq_cut.h"

static const char *msg[] = {"in", "out"};

enum {MAGIC = 123};
struct DQCut {
    int magic;
    int maxc;
    int width, height;
    unsigned char **buf;
    int *status;
};

int dq_cut_ini(int maxc, int width, int height, /**/ DQCut **pq) {
    int i, n;
    DQCut *q;
    EMALLOC(1, &q);
    q->maxc = maxc;
    q->width = width; q->height = height;
    q->magic = MAGIC;
    EMALLOC(maxc, &q->buf);
    n = width * height;
    for (i = 0; i < maxc; i++)
        EMALLOC(n, &q->buf[i]);
    EMALLOC(n, &q->status);
    *pq = q;
    return CUT_OK;
}

int dq_cut_fin(DQCut *q) {
    int maxc, i;
    if (q->magic != MAGIC) {
        fprintf(stderr, "not MAGIC\n");
        return CUT_UNINIT;
    }
    maxc = q->maxc;
    for (i = 0; i < maxc; i++)
        EFREE(q->buf[i]);
    EFREE(q->buf);
    EFREE(q->status);
    EFREE(q);
    return CUT_OK;
}

static int idx(int w, int h, int width) { return h*width + w; }
static int apply(int width, int height, const unsigned char *input,  DQCut *q, int wc, int hc, /**/
                  unsigned char *output, int *pstatus) {
    int width0, height0, w0, h0; /* "small size and coordinates" */
    int h, w, i, i0, status;
    if (q->magic != MAGIC) {
        fprintf(stderr, "not MAGIC\n");
        return CUT_UNINIT;
    }
    width0 = q->width;
    height0 = q->height;
    for (w0 = 0; w0 < width0; w0++)
        for (h0 = 0; h0 < height0; h0++) {
            w = -width0/2  + w0 + wc;
            h = -height0/2 + h0 + hc;
            if (0 > h || h >= height) {status = CUT_STATUS_OUT; goto end; }
            if (0 > w || w >= width)  {status = CUT_STATUS_OUT; goto end; }
            i0 = idx(w0, h0, width0);
            i  = idx(w, h, width);
            output[i0] = input[i];
        }
    status = CUT_STATUS_IN;
end:
    *pstatus = status;
    return CUT_OK;
}

int dq_cut_apply(DQCut *q,
                 int width, int height, const unsigned char *input,
                 int ncom, const int *ww, const int *hh,
                 /**/ unsigned char ***poutput, const int **pstatus) {
    int i, maxc, s;
    maxc = q->maxc;
    if (ncom > maxc) {
        fprintf(stderr, "ncom=%d > maxc=%d\n", ncom, maxc);
        return CUT_SIZE;
    }
    
    for (i = 0; i < ncom; i++) {
        s = apply(width, height, input, q, ww[i], hh[i], /**/ q->buf[i], &q->status[i]);
        if (s != CUT_OK) return s;
    }

    *pstatus = q->status;
    *poutput = q->buf;
    return CUT_OK;
}

const char *dq_cut_status2msg(int s) {
    int n;
    n = sizeof(msg)/sizeof(msg[0]);
    if (0 > s || s >= n) {
        fprintf(stderr, "wrong cut_status: s=%d/n=%d\n", s, n);
        exit(2);
    }
    return msg[s];
}
