D = tools/dng2png tools/dng2tif tools/xi tools/prefix tools/config tools/octave \
    lib/xi lib/error lib/memory lib/stack lib/components lib/cut lib/img/desc lib/img/desc \
    lib/img/format lib/img/log lib/img/io lib/img/pgm lib/api lib/pgm \
    bin/api/main bin/api/main/src bin/api/write \
    bin/api/read bin/api/xi2pgm

E = example/img/desc example/img/format example/pgm example/components example/cut

S = .

include $S/make/dir.mk
