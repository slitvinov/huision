#include <stdio.h>

#include <m3api/xiApi.h>
#include "dq_img_desc.h"

#include "names.h"
#include "msgs.h"

void dq_img_desc2log(XI_IMG_DESC desc) {
    int n, i, v, *pv;
    const char *name;
    fprintf(stderr, "<desc\n");
    #include "vals.h"
    n = sizeof(names)/sizeof(names[0]);
    for (i = 0; i < n; i++) {
        name = names[i];
        pv    = vals[i];
        v     = *pv;
        fprintf(stderr, "%-25s %10d\n", name, v);
    }
    fprintf(stderr, ">desc\n");
}
