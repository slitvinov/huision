#include <m3api/xiApi.h>
#include "dq_img_format.h"

#include "codes.h"
#include "msgs.h"
#include "names.h"

static const char *unk = "(UNKNOWN)";

const char *dq_img_format2msg(XI_IMG_FORMAT f) {
    int n;
    n = sizeof(msgs)/sizeof(msgs[0]);
    if (0 > n || f > n ) return unk;
    return msgs[f];
}

const char *dq_img_format2name(XI_IMG_FORMAT f) {
    int n;
    n = sizeof(names)/sizeof(names[0]);
    if (0 > n || f > n ) return unk;
    return names[f];
}
