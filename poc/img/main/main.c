#include <stdio.h>
#include <m3api/xiApi.h>

enum {DQ_INT, DQ_INT5, DQ_FLOAT, DQ_UNKNOWN};
#include "type.h"
#include "msgs.h"
#include "names.h"

void g_int(const char *n, void *vo) {
    int *pval, val;
    pval = (int*)vo;
    val  = *pval;
    fprintf(stderr, "%-25s %d\n", n, val);
}
void g_int5(const char *n, void *vo) {
    int **pval, *val;
    pval = (int**)vo;
    val  = *pval;
    fprintf(stderr, "%-25s %d %d %d %d %d\n", n,
            val[0], val[1], val[2], val[3], val[4]);
}
void g_float(const char *n, void *vo) {
    float *pval, val;
    pval = (float*)vo;
    val  = *pval;
    fprintf(stderr, "%-25s %g(float)\n", n, val);
}
void g_unknown(const char *n, void *pv) {
    fprintf(stderr, "%-25s\n", n);
}
static void (*wrt[])(const char *n, void*) =
{g_int, g_int5, g_float, g_unknown};

int main() {
    int n, i, t;
    void *v;
    const char *name;
    XI_IMG img;
    #include "val.h"
    n = sizeof(types)/sizeof(types[0]);
    for (i = 0; i < n; i++) {
        t = types[i];
        v = vals[i];
        name = names[i];
        wrt[t](name, v);
    }
}
