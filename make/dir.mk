PREFIX = $(HOME)
XI = /opt/XIMEA

install:
	@install0 () ( cd "$$d" && make install PREFIX=$(PREFIX) XI=$(XI) ); \
	for d in $D; \
	do install0; \
	echo dir "'$$d'"; \
	done

example:
	@install0 () ( cd "$$d" && make PREFIX=$(PREFIX) XI=$(XI) ); \
	for d in $E; \
	do install0; \
	echo dir "'$$d'"; \
	done


.PHONY: install example
